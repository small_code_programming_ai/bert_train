export BERT_BASE_DIR=G:/bert-use-demo/chinese_L-12_H-768_A-12

export MY_DATASET=G:/bert-use-demo/data

python bert/run_classifier.py \
  --data_dir=G:/bert-use-demo/data \
  --task_name=sim \
  --vocab_file=G:/bert-use-demo/chinese_L-12_H-768_A-12/vocab.txt \
  --bert_config_file=G:/bert-use-demo/chinese_L-12_H-768_A-12/bert_config.json \
  --output_dir=/tmp/sim_model/ \
  --do_train=true \
  --do_eval=true \
  --init_checkpoint=G:/bert-use-demo/chinese_L-12_H-768_A-12/bert_model.ckpt \
  --max_seq_length=128 \
  --train_batch_size=32 \
  --learning_rate=5e-5\
  --num_train_epochs=2.0 

PAUSE